import { HP_API_KEY, HP_ROUTE_PREFIX } from '../consts';
import { Row, Col } from 'reactstrap';
import React, { Component } from 'react';

class HpLoader extends Component {
    state = {
        sortingHat: null,
        houses: [], 
        members: [],
        membersDivided: [],
        characters: [],
        charactersDividedbyHouses: [],
    };

    async componentDidMount() {
        const response = await fetch(`${HP_ROUTE_PREFIX}sortingHat`);
        const yourHogwartsHouse = await response.json();
        const responseHouses = await fetch(`${HP_ROUTE_PREFIX}houses?&key=${HP_API_KEY}`);
        const appHpHouses = await responseHouses.json();
        let newState = Object.assign({}, this.state);
        let requestsPerMember = [];
        appHpHouses.forEach(element=>{
            element.members.forEach(member => {
                requestsPerMember.push(
                    `${HP_ROUTE_PREFIX}characters/${member}?&key=${HP_API_KEY}`
                );
                newState.membersDivided.push({
                    house: element._id,
                    houseName: element.name,
                    character: member
                });
                newState.members.push({id: member});
            })
        });

        Promise.all(requestsPerMember.map(url =>
            fetch(url)
              .then(response=>response)                 
              .then(responseJSON=> responseJSON.json())
              .catch(() => {})
          ))
        .then(data => {
            newState.characters.push(data);
            data.forEach(element => {
                appHpHouses.map(house => {
                    house.members.map(member => {
                        if(element !== null && member === element._id){
                            newState.charactersDividedbyHouses.push({
                                member: element.name,
                                house: house.name,
                                houseID: house._id
                            });
                        }
                    });
                });
            });
            this.setState(newState);
        });
        newState.sortingHat = yourHogwartsHouse;
        newState.houses = appHpHouses;
        this.setState(newState);
    }

    getHousesData = () => {
        const houseElement = this.state.houses.map((house,id) => 
            <div key={id}>
                <Col><h3>{house.name}</h3></Col>
                <Col><p>Founder: <br/>{house.founder}</p></Col>
                <Col><p>Colors: <br/>{house.colors.join(", ")}</p></Col>
                <Col><p>Head of house:<br/> {house.headOfHouse}</p></Col>
                <Col><p>House ghost: <br/>{house.houseGhost}</p></Col>
                <Col><p>Values:<br/> {house.values.join(", ")}</p></Col>
                <Col><p>Members: <br/>
                {this.state.charactersDividedbyHouses.map((character, i) => 
                character.houseID == house._id ? character.member+ ' \n' : '\n'
            )}</p></Col>
            </div>
        );
        return houseElement;
    }


    render() {
        const sortingHatMessage =  this.state.sortingHat !== null?`You belong to ${this.state.sortingHat}!`: null;
        let houses = this.getHousesData();

        return (
            <div className="App">
                <p> {sortingHatMessage}</p>
                <Row>
                {houses}
                </Row>
            </div>
        );
    }
  }
  
  export default HpLoader;
  