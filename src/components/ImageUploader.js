import React, { Component } from 'react';
import { Media, FormGroup, Label, Input, Button, Alert } from 'reactstrap';
export const EXTENSIONS = ['jpg','jpeg', 'png'];

class ImageUploader extends Component{
  state = {
    loading: true,
    photo: this.props.photo,
    imageLoaded: 0,
    error: false,
    errorMessage: '',
    successMessage: '',
    selectedFile: null
  }

  onPhotoHandleChange = e => { 
    let file = e.target.files[0];
    const fileName = e.target.files[0].name;
    const returnExt = this.checkExtension(fileName);
    let found = EXTENSIONS.find(element =>element === returnExt[0]);
    if(!found){
      this.setState({
        error: true,
        errorMessage: 'wrong_extension_error'
      })
    }else{
      this.setState({
        error: false,
        errorMessage: ''
      })
    }
    this.setState({
        imageLoaded: 0,
        selectedFile: file
    });
  }

  checkExtension = fileName => (/[.]/.exec(fileName)) ? /[^.]+$/.exec(fileName) : undefined;

  uploadPhoto = () => {
    this.getBase64(this.state.selectedFile).then(
      base64 => this.onPostImage(base64)
    );
  }

  convertPhoto = file =>  {
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
 }

 async getBase64(file) {
    try {
        const base64 = await this.convertPhoto(file);
        return base64;
      }
      catch (e) {
        return;
    }
}
  onPostImage = (base64) => {
    const authData = {
      'photo_base64': base64,
      'user_id': this.props.id
    };
    // api.post({
    //   url: this.props.endpointUrl,
    //   data: authData
    // }, true).then(usersResponse => {
    //     if(usersResponse.status === SUCCESSFUL_STATUS_CODE){
    //       this.setState({ 
    //         loading: false,
    //         photo: usersResponse.data.photo,
    //         error: false,
    //         successMessage: <FormattedMessage id={'success_image_msg'}/>
    //       });
    //     }
    // }).catch (error => {
    //     console.log(error)
    // });
  }

  render = () => {
    let photo = null, photoURL = this.state.photo;
    let successMsg = null, errorMessage = null;
    if(!this.state.error && this.state.successMessage !== ''){
      successMsg = <Alert color="success">{this.state.successMessage}</Alert>;
    }
    if(this.state.error && this.state.errorMessage !== ''){
      errorMessage = <Alert color="danger">{this.state.errorMessage}</Alert>;
    }
    if(this.state.photo){
      photo =<Media object data-src={photoURL} size='small' rounded className="d-block" alt="Photo"/> 
    }
    return (
      <div>
        <FormGroup> 
          { successMsg }
          { errorMessage }
        <Label for="photo">Photo</Label>
          { photo }
        <Input icon='photo' name="photo"
            type="file"  id="photo"
            accept="image/png, image/jpeg"
            onChange={this.onPhotoHandleChange}  />
        </FormGroup>
        <Button className="btn btn-primary" onClick={ this.uploadPhoto } >Upload image</Button>
      </div>
    );
  };
}
export default ImageUploader;