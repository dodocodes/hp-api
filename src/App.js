import React, { Component } from 'react';
import './App.css';
import HelloWorld from './components/HelloWorld';
import HpLoader from './components/HpLoader';
class App extends Component {
  render() {
    return (
      <div className="App">
      {/* <HelloWorld name="World" /> */}
      <HpLoader />
      </div>
    );
  }
}

export default App;
